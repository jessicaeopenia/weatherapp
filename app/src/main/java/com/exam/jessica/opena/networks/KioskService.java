package com.exam.jessica.opena.networks;

import com.exam.jessica.opena.models.request.ObjectHolder;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface KioskService {

    @GET("group?")
    Call<ObjectHolder> getWeatherList(@Query("id") String id, @Query("units") String metric, @Query("appid") String appId);

    @GET("find?")
    Call<ObjectHolder> getCurrentLocWeather(@Query("lat") String latitude,@Query("lon") String longitude, @Query("units") String metric, @Query("appid") String appId);
}
