package com.exam.jessica.opena.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.exam.jessica.opena.R;
import com.exam.jessica.opena.models.Constant;
import com.exam.jessica.opena.models.response.GetWeatherResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherListAdapter extends BaseRecyclerViewAdapter<WeatherListAdapter.WeatherListViewHolder, GetWeatherResponse> {


    public WeatherListAdapter(Context context, List<GetWeatherResponse> data, OnSpecificItemClicked onSpecificItemClicked) {
        super(context, data, onSpecificItemClicked);
    }

    @Override
    public WeatherListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeatherListViewHolder(getItemView(parent, viewType));
    }

    @Override
    protected int getItemLayout() {
        return R.layout.item_weather_list;
    }

    @Override
    public void onBindViewHolder(WeatherListViewHolder holder, final int position) {
        final GetWeatherResponse locationWeather = data.get(position);
        holder.ivLocation.setBackgroundResource(Constant.getListDrawable(locationWeather.getName()));
        holder.tvLocation.setText(locationWeather.getName());
        holder.tvWeatherDesc.setText(locationWeather.getWeather().get(0).getDescription());
        holder.tvActualTemp.setText(locationWeather.getMain().getTemp() + Constant.DEGREE_CELSIUS);
        Glide.with(context).load(Constant.getWeatherIconUrl(locationWeather.getWeather().get(0).getIcon())).into(holder.ivWeatherIcon);
        holder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onSpecificItemClicked!=null){
                    onSpecificItemClicked.onClick(position, locationWeather);
                }
            }
        });
    }

    public class WeatherListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_location)              public ImageView ivLocation;
        @BindView(R.id.iv_weather_icon)              public ImageView ivWeatherIcon;
        @BindView(R.id.tv_location)              public TextView tvLocation;
        @BindView(R.id.tv_weather_desc)              public TextView tvWeatherDesc;
        @BindView(R.id.tv_actual_temp)          public TextView tvActualTemp;
        @BindView(R.id.card_view_item)          public CardView cardViewMain;

        public WeatherListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
