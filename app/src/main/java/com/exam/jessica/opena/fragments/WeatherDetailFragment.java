package com.exam.jessica.opena.fragments;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.exam.jessica.opena.BuildConfig;
import com.exam.jessica.opena.R;
import com.exam.jessica.opena.activities.MainActivity;
import com.exam.jessica.opena.models.Constant;
import com.exam.jessica.opena.models.request.ObjectHolder;
import com.exam.jessica.opena.models.response.GetWeatherResponse;
import com.exam.jessica.opena.networks.KioskClient;
import com.exam.jessica.opena.utilities.AppUtils;
import com.exam.jessica.opena.utilities.SharedPrefDataSession;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherDetailFragment extends BaseFragment {

    @BindView(R.id.iv_location) ImageView ivLocation;
    @BindView(R.id.iv_weather_icon)               ImageView ivWeatherIcon;
    @BindView(R.id.tv_location)               TextView tvLocation;
    @BindView(R.id.tv_weather_desc)               TextView tvWeatherDesc;
    @BindView(R.id.tv_actual_temp)           TextView tvActualTemp;

    @BindView(R.id.tv_weather_in)           TextView tvWeatherIn;
    @BindView(R.id.tv_pressure)           TextView tvPressure;
    @BindView(R.id.tv_humidity)           TextView tvHumidity;
    @BindView(R.id.tv_wind)           TextView tvWind;
    @BindView(R.id.tv_sunrise)           TextView tvSunset;
    @BindView(R.id.tv_sunset)           TextView tvSunrise;


    @BindString(R.string.progress_dialog_loading)         String strLoading;
    @BindString(R.string.progress_dialog_getting_info)         String strGettingInfo;


    private final String SELECTED_LOCATION ="selectedLocation";
    private GetWeatherResponse getWeatherResponse;
    private ProgressDialog progressDialog;
    private final String PAGE_TITLE = WeatherDetailFragment.class.getSimpleName();

    @Override
    protected void initViews() {
        setHasOptionsMenu(true);
        android.support.v7.app.ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        Bundle bundle = getArguments();
        if(bundle!=null) {
            getWeatherResponse = (GetWeatherResponse) bundle.getSerializable(SELECTED_LOCATION);
            actionBar.setTitle(getWeatherResponse.getName());
            setUpData();
        }else{
            onBackPressed();
        }
    }

    private void setUpData(){
        Glide.with(getContext()).load(Constant.getWeatherIconUrl(getWeatherResponse.getWeather().get(0).getIcon())).into(ivWeatherIcon);
        ivLocation.setBackgroundResource(Constant.getListDrawable(getWeatherResponse.getName()));
        tvWeatherDesc.setText(getWeatherResponse.getWeather().get(0).getDescription());
        tvActualTemp.setText(getWeatherResponse.getMain().getTemp() + Constant.DEGREE_CELSIUS);
        tvWeatherIn.setText(getWeatherResponse.getNameWeatherIn());
        tvPressure.setText(getWeatherResponse.getMain().getPressureWithUnit());
        tvHumidity.setText(getWeatherResponse.getMain().getHumidityWithUnit());
        tvSunrise.setText(AppUtils.getDate(getWeatherResponse.getSys().getSunrise()));
        tvSunset.setText(AppUtils.getDate(getWeatherResponse.getSys().getSunset()));
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_weather_detail;
    }

    @Override
    public boolean onBackPressed() {
        sendTrackEvents("Back",PAGE_TITLE);
        back();
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.detail_page_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refresh_page:
                getListOfData();
                sendTrackEvents("Refresh",PAGE_TITLE);
                break;
            case android.R.id.home:
                back();
                sendTrackEvents("Back",PAGE_TITLE);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void back(){
        getBaseActivity().removeFragment(this);
    }

    private void getListOfData(){
        progressDialog = ProgressDialog.show(getBaseActivity(), strLoading, strGettingInfo);
        Callback getWeatherCallback = new Callback<ObjectHolder>() {
            @Override
            public void onResponse(Call<ObjectHolder> call, Response<ObjectHolder> response) {
                if(getContext()!=null) {
                    List<GetWeatherResponse> getWeatherResponses = response.body().getList();
                    getWeatherResponse = getWeatherResponses.get(0);
                    progressDialog.dismiss();
                    setUpData();
                }
            }

            @Override
            public void onFailure(Call<ObjectHolder> call, Throwable t) {
                progressDialog.dismiss();
            }
        };
        KioskClient.getInstance().getService().getWeatherList(getWeatherResponse.getId()+"",Constant.UNITS, BuildConfig.API_KEY).enqueue(getWeatherCallback);
    }
}
