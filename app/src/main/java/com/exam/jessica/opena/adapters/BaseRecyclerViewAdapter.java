package com.exam.jessica.opena.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public abstract class BaseRecyclerViewAdapter<K extends RecyclerView.ViewHolder, V> extends RecyclerView.Adapter<K> {
    protected Context context;
    protected List<V> data;
    protected OnSpecificItemClicked onSpecificItemClicked;

    public BaseRecyclerViewAdapter(Context context, List<V> data, OnSpecificItemClicked onSpecificItemClicked){
        this.context = context;
        this.data = data;
        this.onSpecificItemClicked = onSpecificItemClicked;
    }

    public interface OnSpecificItemClicked{
        void onClick(int position, Object object);
    }

    @Override
    public abstract K onCreateViewHolder(ViewGroup parent, int viewType);

    protected abstract int getItemLayout();

    protected View getItemView(ViewGroup parent, int viewType){
        return LayoutInflater.from(context).inflate(getItemLayout(), parent, false);
    }

    @Override
    public abstract void onBindViewHolder(K holder, int position);

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void notifyDataChange(List<V> datas){
        data = datas;
        notifyDataSetChanged();
    }
}