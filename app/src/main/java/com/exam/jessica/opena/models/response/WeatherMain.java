package com.exam.jessica.opena.models.response;

import com.exam.jessica.opena.models.Constant;

public class WeatherMain {
    private String humidity;
    private String pressure;
    private String temp;
    private String temp_max;
    private String temp_min;

    public String getHumidity() {
        return humidity;
    }

    public String getHumidityWithUnit() {
        return humidity + " "+ Constant.HUMIDITY_UNIT ;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public String getPressureWithUnit(){
        return pressure+ " "+ Constant.PRESSURE_UNIT;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }
}
