package com.exam.jessica.opena.activities;

import android.content.Intent;
import android.os.CountDownTimer;

import com.exam.jessica.opena.R;

public class SplashActivity extends BaseActivity {
    private final long SPLASH_DURATION = 2000;
    private final long INTERVAL= 1000;

    @Override
    protected void initViews() {
        new CountDownTimer(SPLASH_DURATION, INTERVAL) {
            public void onTick(long millisUntilFinished) { }
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }

    @Override
    protected int getFragmentHolderLayout() {
        return 0;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }
}
