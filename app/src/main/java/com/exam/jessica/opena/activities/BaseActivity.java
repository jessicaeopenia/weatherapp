package com.exam.jessica.opena.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.exam.jessica.opena.R;
import com.exam.jessica.opena.fragments.BaseFragment;
import com.exam.jessica.opena.utilities.AppUtils;

import java.util.Stack;

import butterknife.ButterKnife;


public abstract class BaseActivity extends AppCompatActivity {

    protected Stack<BaseFragment> fragments = new Stack<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        initViews();
    }

    protected abstract int getLayout();
    protected abstract void initViews();
    protected abstract int getFragmentHolderLayout();


    public void attachFragment(BaseFragment fragment) {
        attachFragment(fragment, true, false);
    }

    public void attachFragment(BaseFragment fragment, boolean shouldAdd, boolean shouldAnimate) {
        attachFragment(fragment, shouldAdd, shouldAnimate, 0, 0);
    }

    public void attachFragment(BaseFragment fragment, boolean shouldAdd, boolean shouldAnimate, int enter, int exit) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (shouldAnimate) {
            int enterAnim = enter == 0 ? R.anim.enter_right : enter;
            int exitAnim = exit == 0 ? R.anim.exit_right : exit;
            ft.setCustomAnimations(enterAnim, exitAnim);
        }
        fragments.add(fragment);
        if (shouldAdd) {
            ft.add(getFragmentHolderLayout(), fragment).addToBackStack(fragment.getTag());
        } else {
            ft.replace(getFragmentHolderLayout(), fragment).addToBackStack(null);
        }
        ft.commitAllowingStateLoss();
        AppUtils.hideKeyboard(this);
    }

    public void removeFragment() {
        if (fragments != null) {
            fragments.pop();
        }
    }

    public void removeFragment(BaseFragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment).commitAllowingStateLoss();
        fm.popBackStack();
    }
}
