package com.exam.jessica.opena.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.exam.jessica.opena.BuildConfig;
import com.exam.jessica.opena.R;
import com.exam.jessica.opena.activities.MainActivity;
import com.exam.jessica.opena.adapters.BaseRecyclerViewAdapter;
import com.exam.jessica.opena.adapters.WeatherListAdapter;
import com.exam.jessica.opena.models.Constant;
import com.exam.jessica.opena.models.response.GetWeatherResponse;
import com.exam.jessica.opena.models.request.ObjectHolder;
import com.exam.jessica.opena.networks.KioskClient;
import com.exam.jessica.opena.utilities.GPSTracker;
import com.exam.jessica.opena.utilities.SharedPrefDataSession;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherListFragment extends BaseFragment implements BaseRecyclerViewAdapter.OnSpecificItemClicked, View.OnClickListener {

    @BindView(R.id.rv_weather_list)
    RecyclerView rvWeatherList;
    @BindView(R.id.btn_current_location)
    Button btnCurrentLo;

    @BindString(R.string.progress_dialog_loading)
    String strLoading;
    @BindString(R.string.progress_dialog_getting_info)
    String strGettingInfo;

    private WeatherListAdapter weatherListAdapter;
    private List<GetWeatherResponse> weatherResponses = new ArrayList<>();
    private ProgressDialog progressDialog;
    private final String SELECTED_LOCATION = "selectedLocation";
    private final String PAGE_TITLE = WeatherListFragment.class.getSimpleName();

    private double latitude = -1;
    private double longitude = -1;

    @Override
    protected void initViews() {
        setHasOptionsMenu(true);
        weatherListAdapter = new WeatherListAdapter(getContext(), weatherResponses, this);
        rvWeatherList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvWeatherList.setAdapter(weatherListAdapter);
        setUpOfflineData();
        getListOfData();
        btnCurrentLo.setOnClickListener(this);
    }

    private void getListOfData() {
        progressDialog = ProgressDialog.show(getBaseActivity(), strLoading, strGettingInfo);
        Callback getWeatherCallback = new Callback<ObjectHolder>() {
            @Override
            public void onResponse(Call<ObjectHolder> call, Response<ObjectHolder> response) {
                if (getContext() != null) {
                    weatherResponses = response.body().getList();
                    onLoadComplete();
                    if (longitude != -1 && latitude != -1) {
                        getCurrentLocationWeatherData();
                    }
                }
            }

            @Override
            public void onFailure(Call<ObjectHolder> call, Throwable t) {
                progressDialog.dismiss();
            }
        };
        KioskClient.getInstance().getService().getWeatherList(Constant.getCityIds(), Constant.UNITS, BuildConfig.API_KEY).enqueue(getWeatherCallback);
    }

    private void onLoadComplete() {
        weatherListAdapter.notifyDataChange(weatherResponses);
        SharedPrefDataSession.getInstance(getContext()).updateCurrentAdminSession(weatherResponses);
        progressDialog.dismiss();
    }

    private void setUpOfflineData() {
        List<GetWeatherResponse> getWeatherResponses = SharedPrefDataSession.getInstance(getContext()).getCurrentAdminSession();
        weatherListAdapter.notifyDataChange(getWeatherResponses);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_weather_list;
    }

    @Override
    public boolean onBackPressed() {
        getBaseActivity().finish();
        return true;
    }

    @Override
    public void onClick(int position, Object object) {
        GetWeatherResponse weatherResponse = (GetWeatherResponse) object;
        sendTrackEvents("Selected Item City Name : "+ weatherResponse.getName(),PAGE_TITLE);
        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_LOCATION, weatherResponse);
        WeatherDetailFragment detailFragment = new WeatherDetailFragment();
        detailFragment.setArguments(bundle);
        showNewPage(detailFragment, true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            getListOfData();
            sendTrackEvents("Refresh", PAGE_TITLE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_current_location:
                checkPermissions();
                sendTrackEvents("Get Current Location", PAGE_TITLE);
                break;
        }
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                getCurrentLocation();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1000);
            }
        } else {
            getCurrentLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        checkPermissions();
    }

    private void getCurrentLocation() {
        GPSTracker mGPS = new GPSTracker(getContext());
        if (mGPS.canGetLocation()) {
            mGPS.getLocation();
            latitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            getCurrentLocationWeatherData();
        } else {
            Toast.makeText(getContext(), R.string.error_getting_location, Toast.LENGTH_LONG).show();
        }
    }

    private void getCurrentLocationWeatherData() {
        progressDialog = ProgressDialog.show(getBaseActivity(), strLoading, strGettingInfo);
        Callback getWeatherCallback = new Callback<ObjectHolder>() {
            @Override
            public void onResponse(Call<ObjectHolder> call, Response<ObjectHolder> response) {
                if (getContext() != null) {
                    weatherResponses.add(response.body().getList().get(0));
                    onLoadComplete();
                    btnCurrentLo.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ObjectHolder> call, Throwable t) {
                progressDialog.dismiss();
            }
        };
        KioskClient.getInstance().getService().getCurrentLocWeather(latitude + "", longitude + "", Constant.UNITS, BuildConfig.API_KEY).enqueue(getWeatherCallback);

    }

    @Override
    public void onResume() {
        super.onResume();
        android.support.v7.app.ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setTitle("");
    }
}
