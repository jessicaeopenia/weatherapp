package com.exam.jessica.opena.models.request;

import com.exam.jessica.opena.models.response.GetWeatherResponse;

import java.util.List;

public class ObjectHolder {
    private List<GetWeatherResponse> list;
    private String cod;

    public List<GetWeatherResponse> getList() {
        return list;
    }

    public void setList(List<GetWeatherResponse> list) {
        this.list = list;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }
}
