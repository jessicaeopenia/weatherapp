package com.exam.jessica.opena.models.response;

import java.io.Serializable;
import java.util.List;

public class GetWeatherResponse implements Serializable {
    private WeatherMain main;
    private WeatherSys sys;
    private List<WeatherDesc> weather;
    private String visibility;
    private String name;
    private String nameWeatherIn;
    private long id;


    public WeatherMain getMain() {
        return main;
    }

    public void setMain(WeatherMain main) {
        this.main = main;
    }

    public WeatherSys getSys() {
        return sys;
    }

    public void setSys(WeatherSys sys) {
        this.sys = sys;
    }

    public List<WeatherDesc> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherDesc> weather) {
        this.weather = weather;
    }

    public String getVisibility() {
        return visibility;
    }

    public String getName() {
        return name;
    }


    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getNameWeatherIn() {
        return "Weather in "+name + ","+ getSys().getCountry();
    }

    public void setNameWeatherIn(String nameWeatherIn) {
        this.nameWeatherIn = nameWeatherIn;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
