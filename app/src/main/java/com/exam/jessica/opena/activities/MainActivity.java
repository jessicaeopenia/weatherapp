package com.exam.jessica.opena.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.exam.jessica.opena.R;
import com.exam.jessica.opena.fragments.BaseFragment;
import com.exam.jessica.opena.fragments.WeatherListFragment;

public class MainActivity extends BaseActivity {

    private static BaseFragment currentFragment;

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (currentFragment == null) {
            currentFragment = new WeatherListFragment();
        }
        if (savedInstanceState == null) {
            attachFragment(currentFragment);
        }
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected int getFragmentHolderLayout() {
        return R.id.fl_fragment_holder;
    }



}
