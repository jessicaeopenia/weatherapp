package com.exam.jessica.opena.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.exam.jessica.opena.models.response.GetWeatherResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPrefDataSession {

    private static final String ADMIN_SESSIONPREF = "Sf_admin";
    private static final String MAIN_DATA = "main_data";

    private static SharedPrefDataSession sharedPrefUtil;
    private SharedPreferences sharedPreferences;

    private SharedPrefDataSession(Context context) {
        sharedPreferences = context.getSharedPreferences(ADMIN_SESSIONPREF, Context.MODE_PRIVATE);
    }

    public static SharedPrefDataSession getInstance(Context context) {
        if (sharedPrefUtil == null) {
            sharedPrefUtil = new SharedPrefDataSession(context);
        }
        return sharedPrefUtil;
    }

    public void updateCurrentAdminSession(List<GetWeatherResponse> currentAdminSession){
        if(currentAdminSession!=null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            String jsonized = new Gson().toJson(currentAdminSession);
            editor.putString(MAIN_DATA, jsonized);
            editor.commit();
        }
    }

    public List<GetWeatherResponse> getCurrentAdminSession(){
        String jsonAdminSession = sharedPreferences.getString(MAIN_DATA,"[]");
        return new Gson().fromJson(jsonAdminSession, new TypeToken<ArrayList<GetWeatherResponse>>() {}.getType());
    }
}
