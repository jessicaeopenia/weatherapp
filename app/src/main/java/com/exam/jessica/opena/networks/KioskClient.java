package com.exam.jessica.opena.networks;

import android.util.Log;

import com.exam.jessica.opena.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KioskClient {

    private static KioskClient kioskClient;
    private KioskService service;

    private static final int CONNECT_TIMEOUT_MILLIS = 1000 * 1000; // 10s
    private static final int READ_TIMEOUT_MILLIS =  1000 * 1000; // 10s

    private KioskClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(KioskService.class);
    }

    public static KioskClient getInstance(){
        if (kioskClient == null){
            kioskClient = new KioskClient();
        }
        return kioskClient;
    }

    public KioskService getService() {
        return service;
    }

    private OkHttpClient getHttpClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(READ_TIMEOUT_MILLIS,TimeUnit.MILLISECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                Response response = chain.proceed(request);
                int count = 1;
                while (!response.isSuccessful() && count < 3) {
                    Log.e("intercept", "Request Retry - " + count);
                    count++;
                    response = chain.proceed(request);
                }
                return response;
            }
        });

        return httpClient.build();
    }

}