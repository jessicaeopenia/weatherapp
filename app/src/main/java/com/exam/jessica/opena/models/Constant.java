package com.exam.jessica.opena.models;

import android.support.design.widget.AppBarLayout;

import com.exam.jessica.opena.R;

public class Constant {
    public static final String LONDON= "London";
    public static final String LONDON_ID= "2643743";
    public static final String PRAGUE= "Prague";
    public static final String PRAGUE_ID= "3067696";
    public static final String SAN_FRANCISCO= "San Francisco";
    public static final String SAN_FRANCISCO_ID= "5391959";
    public static final String UNITS= "metric";
    public static final String COMMA= ",";
    public static final String ICON_BASE_URL= "http://openweathermap.org/img/w/";

    public static final String DEGREE_CELSIUS= "°C";
    public static final String PRESSURE_UNIT= "hpa";
    public static final String WIND_UNIT= "m/s";
    public static final String HUMIDITY_UNIT= "%";

    public static int getListDrawable(String cityName){
        int imgDrawable= 0;
        switch (cityName){
            case LONDON:
                imgDrawable = R.drawable.ic_london;
                break;
            case PRAGUE:
                imgDrawable = R.drawable.ic_prague;
                break;
            case SAN_FRANCISCO:
                imgDrawable = R.drawable.ic_san_francisco;
                break;
            default:
                imgDrawable = R.drawable.ic_background;
                break;
        }
        return imgDrawable;
    }

    public static String getCityIds(){
        StringBuilder cityIds = new StringBuilder();
        cityIds.append(LONDON_ID);
        cityIds.append(COMMA);
        cityIds.append(PRAGUE_ID);
        cityIds.append(COMMA);
        cityIds.append(SAN_FRANCISCO_ID);
        return cityIds.toString();
    }

    public static String getWeatherIconUrl(String iconId){
        StringBuilder iconUrl = new StringBuilder();
        iconUrl.append(ICON_BASE_URL);
        iconUrl.append(iconId);
        iconUrl.append(".png");
        return iconUrl.toString();
    }
}
