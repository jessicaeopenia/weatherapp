package com.exam.jessica.opena.fragments;

import android.os.BadParcelableException;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.exam.jessica.opena.R;
import com.exam.jessica.opena.activities.BaseActivity;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindString;
import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    @BindString(R.string.hint_press_back_to_exit) String hintPressBackAgain;

    protected View mainView;
    private AtomicBoolean shouldExit = new AtomicBoolean(false);
    private final int MAX_DELAY = 2000;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        adjustWindowState();
        mainView = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, mainView);
        initViews();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        return mainView;
    }

    protected void adjustWindowState() {
        getBaseActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    protected abstract void initViews();

    protected abstract int getLayout();


    public abstract boolean onBackPressed();

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public void showNewPage(BaseFragment baseFragment, boolean shouldAdd){
        getBaseActivity().attachFragment(baseFragment, shouldAdd, true, R.anim.enter_right, R.anim.exit_right);
    }

    protected void checkBackPress() {
        if (!shouldExit.get()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    shouldExit.set(false);
                }
            }, MAX_DELAY);
            shouldExit.set(true);
            Toast.makeText(getContext(), hintPressBackAgain, Toast.LENGTH_SHORT).show();
        } else {
            getActivity().finish();
        }
    }

    protected void sendTrackEvents(String action, String page){
        Bundle params = new Bundle();
        params.putString("page", page);
        sendFirebaseEvents(params,action);
    }

    protected void sendFirebaseEvents(Bundle params, String eventAction){
        mFirebaseAnalytics.logEvent(eventAction, params);
    }
}
